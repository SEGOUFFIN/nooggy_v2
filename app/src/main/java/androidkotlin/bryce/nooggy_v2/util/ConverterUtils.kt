package androidkotlin.bryce.nooggy_v2.util

import android.graphics.Color

object ConverterUtils {

    fun fromGradeToColor(grade: String): Int {
        return when (grade) {
            "a" -> Color.argb(255, 95, 128, 4)
            "b" -> Color.argb(255, 182, 227, 57)
            "c" -> Color.argb(255, 250, 213, 2)
            "d" -> Color.argb(255, 255, 131, 23)
            "e" -> Color.argb(255, 220, 18, 18)
            else -> Color.TRANSPARENT
        }
    }
}