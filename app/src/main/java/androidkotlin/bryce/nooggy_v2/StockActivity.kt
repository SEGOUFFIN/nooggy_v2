package androidkotlin.bryce.nooggy_v2


import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.icu.text.SimpleDateFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_stock.*
import java.util.*
import com.google.zxing.integration.android.IntentIntegrator



class StockActivity : AppCompatActivity() {

    private lateinit var mViewModel: MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val recyclerView = produit_recycler_view_stock
        val adapter = ProduitAdapterStock(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        mViewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        mViewModel.getAllStock().observe(this, Observer {
            adapter.updateList(it)
            if (it.isEmpty()) {
                empty_stock_screen.visibility = View.VISIBLE
            } else {
                empty_stock_screen.visibility = View.GONE
        }})

        adapter.setOnItemClickListener(object: ProduitAdapterStock.ItemClickListener {
            override fun onClick(position: Int, view: View, produit: Produit) {
                itemClicked(view, produit)
            }
        })
    }

    private fun itemClicked(view: View, produit: Produit) {
        val deleteDialog = Dialog(view.context, R.style.DialogStyle)
        deleteDialog.setContentView(R.layout.delete_dialog)
        deleteDialog.setCancelable(true)
        deleteDialog.show()

        Glide.with(view.context).load(produit.urlImage).into(deleteDialog.findViewById(R.id.delete_dialog_image))

        deleteDialog.findViewById<View>(R.id.delete_dialog_delete_button).setOnClickListener {
            mViewModel.deleteFromStock(produit.id)
            deleteDialog.dismiss()
        }

        deleteDialog.findViewById<View>(R.id.delete_dialog_add_button).setOnClickListener {
            mViewModel.addToAchatFromStock(produit.id)
            deleteDialog.dismiss()
        }

        deleteDialog.findViewById<View>(R.id.delete_dialog_modify_button).setOnClickListener {
            modifyButtonClicked(deleteDialog, produit)
        }
    }

    private fun modifyButtonClicked(
        deleteDialog: Dialog,
        produit: Produit
    ) {
        deleteDialog.setContentView(R.layout.add_dialog)

        var datePeremptionProduit = App.NULL_DATE
        var quantiteProduit = 1
        var prixProduit = "Prix :  €"

        //Récupère les vues
        val quantiteView = deleteDialog.findViewById<EditText>(R.id.quantite_add_dialog)
        val prixView = deleteDialog.findViewById<EditText>(R.id.prix_add_dialog)
        val dateView = deleteDialog.findViewById<TextView>(R.id.date_scan_add)

        //Mets les valeurs du produit existant dans les vues
        quantiteView.setText(produit.quantite.toString())
        prixView.setText(produit.prix.split(":")[1].trimEnd('€').trim())
        if (produit.datePeremption != App.NULL_DATE) {
            dateView.setText(java.text.SimpleDateFormat("dd/MM/yyyy").format(produit.datePeremption))
            datePeremptionProduit = produit.datePeremption
        } else {
            dateView.setText("Date de péremption")
        }

        deleteDialog.findViewById<TextView>(R.id.date_scan_add).setOnClickListener {
            val calendar = Calendar.getInstance()
            val onDatePicked = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                datePeremptionProduit = calendar.time
                it.findViewById<TextView>(R.id.date_scan_add).text =
                    SimpleDateFormat("dd/MM/yyyy").format(datePeremptionProduit)
            }

            var year = 0
            var month = 0
            var dayOfMonth = 0
            if (produit.datePeremption != App.NULL_DATE) {
                calendar.time = Date(produit.datePeremption.time)
                year = calendar.get(Calendar.YEAR)
                month = calendar.get(Calendar.MONTH)
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            } else {
                calendar.time = Calendar.getInstance().getTime()
                year = calendar.get(Calendar.YEAR)
                month = calendar.get(Calendar.MONTH)
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            }
            val datePickerDialog = DatePickerDialog(
                it.context, onDatePicked,
                year, month, dayOfMonth
            )
            // afficher la dialog
            datePickerDialog.show()
        }

        deleteDialog.findViewById<View>(R.id.bouton_add_final_dialog_scan).setOnClickListener {
            try {
                quantiteProduit =
                    deleteDialog.findViewById<EditText>(R.id.quantite_add_dialog).text.toString().toInt()
            } catch (e: NumberFormatException) {
                quantiteProduit = 1
            }
            prixProduit = "Prix : ${deleteDialog.findViewById<EditText>(R.id.prix_add_dialog).text} €"

            produit.quantite = quantiteProduit
            produit.prix = prixProduit
            produit.datePeremption = datePeremptionProduit
            if (produit.quantite == 0) {
                mViewModel.deleteFromStock(produit.id)
            }
            mViewModel.updateProduit(produit)

            Toast.makeText(it.context, "Produit mis à jour", Toast.LENGTH_SHORT).show()
            deleteDialog.dismiss()
        }
    }
}
