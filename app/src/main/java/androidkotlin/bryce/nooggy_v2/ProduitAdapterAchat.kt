package androidkotlin.bryce.nooggy_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidkotlin.bryce.nooggy_v2.util.ConverterUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_achat.view.*


class ProduitAdapterAchat : RecyclerView.Adapter<ProduitAdapterAchat.ViewHolder>(){


    private var mProduits: List<Produit>? = null
    private lateinit var mListener: ItemClickListener

    //définition du ViewHolder pour l'utiliser dans le super constructeur
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnLongClickListener {

        val achatImageView = itemView.image_achat
        val nomProduitView = itemView.nom_produit_achat
        val marqueProduitView = itemView.marque_produit_achat
        val noteNutritionView = itemView.note_nutrition_bouton_achat


        init {
            itemView.setOnLongClickListener(this)
        }

        override fun onLongClick(view: View): Boolean {
            mListener.onClick(adapterPosition, view, mProduits!![adapterPosition])
            return true
        }

    }

    // permet de convertir le layout item_achat en objet Kotlin que l'on peut manipuler
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.item_achat, parent, false)
        return ViewHolder(viewItem)
    }



    override fun onBindViewHolder(holder: ProduitAdapterAchat.ViewHolder, position: Int) {
        val viewContext: Context = holder.achatImageView.context
        if (mProduits != null) {
            val produitCourant: Produit = mProduits!![position]

            val options = RequestOptions().placeholder(R.drawable.placeholder)
            Glide.with(viewContext).setDefaultRequestOptions(options).load(produitCourant.urlImage)
                .into(holder.achatImageView)

            holder.nomProduitView.text = produitCourant.nom

            holder.marqueProduitView.text = produitCourant.marque

            holder.noteNutritionView.background.setTint(
                ConverterUtils.fromGradeToColor(
                    produitCourant.noteNutrition
                )
            )
        } else {

            val options = RequestOptions().placeholder(R.drawable.placeholder)
            Glide.with(viewContext).setDefaultRequestOptions(options)
                .load(viewContext.getString(R.string.image_not_found_url)).into(holder.achatImageView)
            // remove the placeholder (optional); read comments below
            holder.achatImageView.setImageDrawable(null)

            holder.nomProduitView.text = "Nom : "

            holder.marqueProduitView.text = "Marque : "
        }
    }

    fun updateList(produit: List<Produit>?) {
        mProduits = produit
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int{
        return if (mProduits != null) {
            mProduits!!.size
        } else 0
    }

    fun setOnItemClickListener(clickListener: ItemClickListener) {
        mListener = clickListener
    }

    interface ItemClickListener {
        fun onClick(position: Int, view: View, produit: Produit)
    }
}