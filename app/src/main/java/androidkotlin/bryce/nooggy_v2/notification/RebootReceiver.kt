package androidkotlin.bryce.nooggy_v2.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class RebootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        //Reprogramme l'envoi quotidien des notifications si l'appareil a été redémarré ou
        // que l'application a été mise à jour
        if (intent.action?.equals(Intent.ACTION_BOOT_COMPLETED, ignoreCase = true) == true
            || intent.action?.equals(Intent.ACTION_MY_PACKAGE_REPLACED, ignoreCase = true) == true
        ) {
            NooggyNotificationManager.schedulePeriodicNotifications(context)
        }
    }
}