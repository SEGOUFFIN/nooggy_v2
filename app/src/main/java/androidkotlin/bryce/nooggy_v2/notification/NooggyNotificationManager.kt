package androidkotlin.bryce.nooggy_v2.notification

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.RemoteViews
import androidkotlin.bryce.nooggy_v2.R
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.NotificationTarget
import org.joda.time.DateTime


object NooggyNotificationManager {
    private const val GROUP_NOTIFICATION_ID = 1
    private const val NOTIFICATION_HOUR = 8
    private const val NOTIFICATION_MINUTE = 30

    fun sendExpirationNotification(
        context: Context,
        produit: Produit
    ) {
        val remoteView =
            RemoteViews(context.packageName, R.layout.notification_expiration)
        remoteView.setTextViewText(R.id.title_expiration_notification, produit.nom)
        remoteView.setTextViewText(
            R.id.content_text_expiration_notification,
            "Arrive à péremption Aujourd'hui"
        )

        val channelId = createExpirationNotificationChannel(context)
        val groupId = "expirationGroup"
        val notificationTag = "expirationNotification.${produit.id}"
        val notificationId = produit.id

        val notification = NotificationCompat.Builder(context, channelId).apply {
            setContentTitle(produit.nom)
            setContentText("Arrive à péremption Aujourd'hui")
            setSmallIcon(R.mipmap.ic_peremption)
            setColor(context.resources.getColor(R.color.colorPrimary, context.theme))
            setStyle(NotificationCompat.DecoratedCustomViewStyle())
            setCustomBigContentView(remoteView)
            setPriority(NotificationCompat.PRIORITY_LOW)
            setOnlyAlertOnce(true)
            setCategory(Notification.CATEGORY_STATUS)
            setChannelId(channelId)
            setGroup(groupId)
        }.build()

        val notificationTarget = NotificationTarget(
            context,
            R.id.image_expiration_notification,
            remoteView,
            notification,
            notificationId,
            notificationTag
        )

        Glide
            .with(context.applicationContext)
            .asBitmap()
            .load(produit.urlImage)
            .into(notificationTarget)

        val notificationManager = NotificationManagerCompat.from(context.applicationContext)
        notificationManager.notify(notificationTag, notificationId, notification)
    }

    fun sendExpirationSummaryNotification(context: Context) {
        val channelId = createExpirationNotificationChannel(context)
        val groupId = "expirationGroup"

        val notification = NotificationCompat.Builder(context, channelId).apply {
            setSmallIcon(R.mipmap.ic_peremption)
            setStyle(NotificationCompat.InboxStyle().setSummaryText("Péremption"))
            setColor(context.resources.getColor(R.color.colorPrimary, context.theme))
            setGroup(groupId)
            setGroupSummary(true)
        }.build()
        notification.flags = Notification.FLAG_GROUP_SUMMARY

        val notificationManager = NotificationManagerCompat.from(context.applicationContext)
        notificationManager.notify(groupId, GROUP_NOTIFICATION_ID, notification)
    }

    private fun createExpirationNotificationChannel(context: Context): String {
        val channelId = "expirationNotificationChannel"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName = "Péremption des produits"
            val channelDescription = "Vous prévient quand vos produits arrivent à péremption"

            val channel =
                NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_LOW
                ).apply {
                    description = channelDescription
                    setShowBadge(false)
                }

            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        return channelId
    }

    fun schedulePeriodicNotifications(context: Context) {
        val intent = Intent(context, ExpirationNotificationReceiver::class.java)
        val isNotificationScheduled =
            PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE) != null

        // Si la notification n'est pas déjà programmée, alors on la programme
        if (!isNotificationScheduled) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as? AlarmManager
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)

            val time =
                DateTime.now().withTimeAtStartOfDay().plusHours(NOTIFICATION_HOUR).plusMinutes(
                    NOTIFICATION_MINUTE
                ).millis

            alarmManager?.setRepeating(
                AlarmManager.RTC,
                time,
                AlarmManager.INTERVAL_DAY,
                pendingIntent
            )
        }
    }
}