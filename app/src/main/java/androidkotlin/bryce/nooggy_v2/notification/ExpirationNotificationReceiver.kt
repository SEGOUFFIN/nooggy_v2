package androidkotlin.bryce.nooggy_v2.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidkotlin.bryce.nooggy_v2.data.ProduitRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.joda.time.DateTime

class ExpirationNotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val repository = ProduitRepository()

        GlobalScope.launch {
            withContext(IO) {
                val produits = repository.getAllStockList()

                var hasExpiredProducts = false
                for (produit in produits) {
                    if (DateTime.now().dayOfYear == DateTime(produit.datePeremption.time).dayOfYear && DateTime.now().year == DateTime(
                            produit.datePeremption.time
                        ).year
                    ) {
                        hasExpiredProducts = true
                        NooggyNotificationManager.sendExpirationNotification(context, produit)
                    }
                }
                if (hasExpiredProducts) {
                    NooggyNotificationManager.sendExpirationSummaryNotification(context)
                }
            }
        }
    }

}