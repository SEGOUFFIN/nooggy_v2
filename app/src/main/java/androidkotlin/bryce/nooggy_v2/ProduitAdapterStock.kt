package androidkotlin.bryce.nooggy_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidkotlin.bryce.nooggy_v2.util.ConverterUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_stock.view.*
import java.text.SimpleDateFormat
import java.util.*


class ProduitAdapterStock (context: Context) : RecyclerView.Adapter<ProduitAdapterStock.ViewHolder>(){


    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    private var mProduits: List<Produit>? = null

    private lateinit var mListener: ItemClickListener

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnLongClickListener {

        val stockImageView = itemView.image_stock
        val datePeremptionView = itemView.date_peremption_stock
        val prixProduitView = itemView.prix_produit_stock
        val quantiteProduitView = itemView.quantite_produit_stock
        val nomProduitView = itemView.nom_produit_stock
        val noteNutritionView = itemView.note_nutrition_bouton_stock

        init {
            itemView.setOnLongClickListener(this)
        }

        override fun onLongClick(view: View): Boolean {
            mListener.onClick(adapterPosition, view, mProduits!![adapterPosition])
            return true
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = mInflater.inflate(R.layout.item_stock, parent, false)
        return ViewHolder(viewItem)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val viewContext: Context = holder.stockImageView.context

        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.DAY_OF_YEAR, 5)
        val dateAlerte = calendar.time

        if (mProduits != null) {
            val produitCourant: Produit = mProduits!![position]

            val options = RequestOptions().placeholder(R.drawable.placeholder)
            Glide.with(viewContext).setDefaultRequestOptions(options).load(produitCourant.urlImage)
                .into(holder.stockImageView)
            if (produitCourant.datePeremption != App.NULL_DATE) {
                if (produitCourant.datePeremption.time < Date().time) {
                    holder.datePeremptionView.setTextColor(viewContext.getColor(R.color.colorRed))
                }
                else if (produitCourant.datePeremption <= dateAlerte){
                        holder.datePeremptionView.setTextColor(viewContext.getColor(R.color.colorOrange))
                }
                  else {
                    holder.datePeremptionView.setTextColor(viewContext.getColor(android.R.color.darker_gray))
                }

                holder.datePeremptionView.text =
                    "Date de peremption : " + SimpleDateFormat("dd/MM/yyyy").format(produitCourant.datePeremption)
            } else {

                holder.datePeremptionView.setTextColor(viewContext.getColor(android.R.color.darker_gray))
                holder.datePeremptionView.text = "Date de péremption non renseignée"

            }

            holder.prixProduitView.text = produitCourant.prix

            holder.quantiteProduitView.text = "Quantité : ${produitCourant.quantite}"

            holder.nomProduitView.text = produitCourant.nom

            holder.noteNutritionView.background.setTint(
                ConverterUtils.fromGradeToColor(
                    produitCourant.noteNutrition
                )
            )
        }
    }


    fun updateList(produit: List<Produit>?) {
        mProduits = produit
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int{
        return if (mProduits != null) {
            mProduits!!.size
        } else 0
    }


    fun setOnItemClickListener(clickListener: ItemClickListener) {
        mListener = clickListener
    }


    interface ItemClickListener {
        fun onClick(position: Int, view: View, produit: Produit)

    }
}