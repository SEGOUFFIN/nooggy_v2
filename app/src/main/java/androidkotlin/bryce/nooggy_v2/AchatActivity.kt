package androidkotlin.bryce.nooggy_v2

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_achat.*


class AchatActivity : AppCompatActivity() {

    private lateinit var mViewModel: MyViewModel
    private var mListeAchats: List<Produit>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_achat)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val recyclerView = produit_recycler_view_achat
        val adapter = ProduitAdapterAchat()

        adapter.setOnItemClickListener(object : ProduitAdapterAchat.ItemClickListener {

            override fun onClick(position: Int, view: View, produit: Produit) {

                val deleteDialog = Dialog(view.context, R.style.DialogStyle)
                deleteDialog.setContentView(R.layout.delete_dialog_achat)
                deleteDialog.setCancelable(true)
                deleteDialog.show()



                Glide.with(view.context).load(produit.urlImage)
                    .into(deleteDialog.findViewById(R.id.delete_dialog_image))

                deleteDialog.findViewById<View>(R.id.delete_dialog_delete_button)
                    .setOnClickListener {
                        mViewModel.deleteToAchat(produit.id)
                        Toast.makeText(it.context, "Produit supprimé", Toast.LENGTH_SHORT).show()
                        deleteDialog.dismiss()
                    }

            }
        })

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        mViewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)
        mViewModel.getAllAchats().observe(this, Observer {
            adapter.updateList(it)
            mListeAchats = it
            if (it.isEmpty()) {
                empty_achat_screen.visibility = View.VISIBLE
            } else {
                empty_achat_screen.visibility = View.GONE
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.achat_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.achat_menu -> {
                startListSharing()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun startListSharing() {
        val listeAchats = mListeAchats
        var message = "Liste de courses :\n"

        if (listeAchats != null && !listeAchats.isEmpty()) {
            for (element in listeAchats) {
                message += "${element.nom} (${element.marque})\n"
            }
            message = message.substring(0, message.length - 1)

            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(
                Intent.createChooser(
                    sharingIntent,
                    "Partagez votre liste"
                )
            )
        } else {
            Toast.makeText(this, "Liste d'achats vide", Toast.LENGTH_SHORT).show()
        }
    }
}
