package androidkotlin.bryce.nooggy_v2

import android.content.Context
import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidkotlin.bryce.nooggy_v2.util.ConverterUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_stats.view.*


class ProduitAdapterStat (context: Context)
// héritage de la classe parent, appel du super constructeur
    : androidx.recyclerview.widget.RecyclerView.Adapter<ProduitAdapterStat.ViewHolder>(){

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var mProduits: List<Produit>? = null
    private lateinit var mListener: ItemClickListener


    //définition du ViewHolder pour l'utiliser dans le super constructeur
    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnLongClickListener {

        val statImageView = itemView.image_stats
        val nomProduitView = itemView.nom_produit_stats
        val marqueProduitView = itemView.marque_produit_stats
        val dateAchatView = itemView.date_achat_stats
        val datePeremptionView = itemView.date_peremption_stats
        val noteNutritionView = itemView.note_nutrition_bouton_stats

        init {
            itemView.setOnLongClickListener(this)
        }

        override fun onLongClick(view: View): Boolean {
            mListener.onClick(adapterPosition, view, mProduits!![adapterPosition])
            return true
        }


     }

    // permet de convertir le layout item_stats en objet Kotlin que l'on peut manipuler
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = mInflater.inflate(R.layout.item_stats, parent, false)
        return ViewHolder(viewItem)
    }



    override fun onBindViewHolder(holder: ProduitAdapterStat.ViewHolder, position: Int) {
        val viewContext: Context = holder.statImageView.context
        if (mProduits != null) {
            val produitCourant: Produit = mProduits!![position]

            val options = RequestOptions().placeholder(R.drawable.placeholder)
            Glide.with(viewContext).setDefaultRequestOptions(options).load(produitCourant.urlImage)
                .into(holder.statImageView)


            holder.nomProduitView.text = produitCourant.nom

            holder.marqueProduitView.text = produitCourant.marque

            holder.noteNutritionView.background.setTint(
                ConverterUtils.fromGradeToColor(
                    produitCourant.noteNutrition
                )
            )

            holder.dateAchatView.text =
                "Date d'achat : " + SimpleDateFormat("dd/MM/yyyy").format(produitCourant.dateAchat)

            if (produitCourant.datePeremption != App.NULL_DATE) {
                holder.datePeremptionView.text =
                    "Date de peremption : " + SimpleDateFormat("dd/MM/yyyy").format(produitCourant.datePeremption)
            } else {
                holder.datePeremptionView.text = "date de péremption non renseignée"
            }

        } else {

            val options = RequestOptions().placeholder(R.drawable.placeholder)
            Glide.with(viewContext).setDefaultRequestOptions(options)
                .load(viewContext.getString(R.string.image_not_found_url)).into(holder.statImageView)
            // remove the placeholder (optional); read comments below
            holder.statImageView.setImageDrawable(null)

            holder.nomProduitView.text = "Nom : "

            holder.marqueProduitView.text = "Marque : "
        }
    }

    fun updateList(produit: List<Produit>?) {
        mProduits = produit
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int{
        return if (mProduits != null) {
            mProduits!!.size
        } else 0
    }

    fun setOnItemClickListener(clickListener: ItemClickListener) {
        mListener = clickListener
    }

    interface ItemClickListener {
        fun onClick(position: Int, view: View, produit: Produit)
    }


}