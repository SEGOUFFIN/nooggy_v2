package androidkotlin.bryce.nooggy_v2


import android.app.Dialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.widget.Toast
import androidkotlin.bryce.nooggy_v2.data.Produit
import kotlinx.android.synthetic.main.activity_stats.*
import androidkotlin.bryce.nooggy_v2.data.ProduitRepository
import com.bumptech.glide.Glide
import androidx.annotation.NonNull
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.MenuItem


class StatsActivity : AppCompatActivity() {

    private lateinit var mViewModel: MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val recyclerView = produit_recycler_view_stats
        val adapter = ProduitAdapterStat(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        mViewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        mViewModel.getAllProduits(ProduitRepository.SortingMethod.BUYING_DATE)
            .observe(this, Observer {
                adapter.updateList(it)
                if (it.isEmpty()) {
                    empty_stat_screen.visibility = View.VISIBLE
                } else {
                    empty_stat_screen.visibility = View.GONE
            }})
        bottom_bar.selectedItemId = R.id.sort_date_achat

        adapter.setOnItemClickListener(object : ProduitAdapterStat.ItemClickListener {
            override fun onClick(position: Int, view: View, produit: Produit) {
                val deleteDialog = Dialog(view.context, R.style.DialogStyle)
                deleteDialog.setContentView(R.layout.delete_dialog_stats)
                deleteDialog.setCancelable(true)
                deleteDialog.show()

                Glide.with(view.context).load(produit.urlImage)
                    .into(deleteDialog.findViewById(R.id.delete_dialog_image))

                deleteDialog.findViewById<View>(R.id.delete_dialog_delete_button)
                    .setOnClickListener {
                        mViewModel.deleteProduit(produit)
                        Toast.makeText(
                            it.context,
                            "Produit supprimé de la base de données",
                            Toast.LENGTH_SHORT
                        ).show()
                        deleteDialog.dismiss()
                    }

                deleteDialog.findViewById<View>(R.id.delete_dialog_add_button).setOnClickListener {
                    mViewModel.addToAchat(produit.id)
                    Toast.makeText(
                        it.context,
                        "Produit ajoué à la liste d'achat",
                        Toast.LENGTH_SHORT
                    ).show()
                    deleteDialog.dismiss()
                }

                deleteDialog.findViewById<View>(R.id.delete_dialog_add_stats_button).setOnClickListener {
                    mViewModel.addToStockFromStats(produit.id)
                    Toast.makeText(
                        it.context,
                        "Produit ajouté au stock",
                        Toast.LENGTH_SHORT
                    ).show()
                    deleteDialog.dismiss()
                }
            }
        })

        bottom_bar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.sort_alpha -> mViewModel.getAllProduits(ProduitRepository.SortingMethod.ALPHABETICAL).observe(
                    this,
                    Observer { list ->
                        adapter.updateList(list)
                    })

                R.id.sort_marque -> mViewModel.getAllProduits(ProduitRepository.SortingMethod.BRAND).observe(
                    this,
                    Observer { list ->
                        adapter.updateList(list)
                    })

                R.id.sort_date_achat -> mViewModel.getAllProduits(ProduitRepository.SortingMethod.BUYING_DATE).observe(
                    this,
                    Observer { list ->
                        adapter.updateList(list)
                    })

                R.id.sort_date_peremption -> mViewModel.getAllProduits(ProduitRepository.SortingMethod.EXPIRATION_DATE).observe(
                    this,
                    Observer { list ->
                        adapter.updateList(list)
                    })
            }
            true
        }
    }
}
