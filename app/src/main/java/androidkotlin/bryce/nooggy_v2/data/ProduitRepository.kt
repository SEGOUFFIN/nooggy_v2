package androidkotlin.bryce.nooggy_v2.data

import androidkotlin.bryce.nooggy_v2.App
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext


class ProduitRepository {

    enum class SortingMethod {
        ALPHABETICAL, BRAND, BUYING_DATE, EXPIRATION_DATE
    }

    private val mProduitDao: ProduitDao = App.database.produitDao()

    fun getAllProduits(method: SortingMethod): LiveData<List<Produit>> {
        return when (method) {
            SortingMethod.ALPHABETICAL -> mProduitDao.sortByName()
            SortingMethod.BRAND -> mProduitDao.sortByBrand()
            SortingMethod.BUYING_DATE -> mProduitDao.getAllProduits()
            SortingMethod.EXPIRATION_DATE -> mProduitDao.sortByExpirationDate()
        }
    }

    fun getAllAchats() = mProduitDao.getAllAchats()
    fun getAllStock() = mProduitDao.getAllStock()
    suspend fun getAllStockList() = mProduitDao.getAllStockList()

    fun getPrixByName(name: String): String = runBlocking {
        getPrixByNamesuspend(name)
    }

    private suspend fun getPrixByNamesuspend(name: String): String =
        withContext(IO) {
            mProduitDao.getPrixByName(name)
        }


    fun addToAchatFromStock(id: Int) {
        GlobalScope.launch { addToAchatFromStockSuspend(id) }
    }

    private suspend fun addToAchatFromStockSuspend(id: Int) {
        withContext(IO) {
            mProduitDao.deleteFromStock(id)
            mProduitDao.addToAchat(id)
        }
    }

    fun addToStockFromStats(id: Int) {
        GlobalScope.launch { addToStockFromStatsSuspend(id) }
    }

    private suspend fun addToStockFromStatsSuspend(id: Int) {
        withContext(IO) {
            mProduitDao.addToStock(id)
        }
    }

    fun addToAchat(id: Int) {
        GlobalScope.launch { addToAchatSuspend(id) }
    }

    private suspend fun addToAchatSuspend(id: Int) {
        withContext(IO) {
            mProduitDao.addToAchat(id)
        }
    }


    fun deleteToAchat(id: Int) {
        GlobalScope.launch { deleteToAchatSuspend(id) }
    }

    private suspend fun deleteToAchatSuspend(id: Int) {
        withContext(IO) {
            mProduitDao.deleteToAchat(id)

        }
    }


    fun deleteProduit(produit: Produit) {
        GlobalScope.launch { deleteSuspend(produit) }
    }

    private suspend fun deleteSuspend(produit: Produit) {
        withContext(IO) {
            mProduitDao.deleteProduit(produit)
        }
    }


    fun deleteFromStock(id: Int) {
        GlobalScope.launch { deleteFromStockSuspend(id) }
    }


    private suspend fun deleteFromStockSuspend(id: Int) {
        withContext(IO) {
            mProduitDao.deleteFromStock(id)

        }
    }


    fun addFromScan(produit: Produit) {
        GlobalScope.launch {
            addFromScanSuspend(produit)
        }
    }

    private suspend fun addFromScanSuspend(produit: Produit) {
        withContext(IO) {
            mProduitDao.deleteToAchatByName(produit.nom)
            mProduitDao.insertProduit(produit)

        }
    }


    fun updateProduit(produit: Produit) {
        GlobalScope.launch { updateProduitSuspend(produit) }
    }

    private suspend fun updateProduitSuspend(produit: Produit) {
        withContext(IO) {
            mProduitDao.updateProduit(
                produit.id,
                produit.quantite,
                produit.prix,
                produit.datePeremption
            )

        }
    }
}