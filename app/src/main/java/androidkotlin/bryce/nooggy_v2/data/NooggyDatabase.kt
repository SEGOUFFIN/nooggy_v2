package androidkotlin.bryce.nooggy_v2.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
// Pour créer notre base de donnée il faut, à la fois,
// hériter notre classe de RoomDataBase et ajouter l’annotation @Database

//paramètres de l'annotation = le type d'entité et la version

@Database(entities = [Produit::class], version = 5, exportSchema = false )


@TypeConverters(Converters::class)

abstract class NooggyDatabase : RoomDatabase() {

    // fonction abstraite qui pourra créé un produitDao
    // Une classe abstraite permet de factoriser des comportements et sert de base à des classes qui en héritent.
    // Une classe abstraite n’est pas instanciable.
    abstract fun produitDao() : ProduitDao
}

// Room est une couche d’abstraction à SQLite qui facilite la gestion de votre base