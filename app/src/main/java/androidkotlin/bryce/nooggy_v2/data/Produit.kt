package androidkotlin.bryce.nooggy_v2.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidkotlin.bryce.nooggy_v2.App
import java.util.*



@Entity(
    tableName = "produits"
)
data class Produit(
    var nom: String,
    @ColumnInfo(name = "nom_marque")
    var marque: String,
    var urlImage: String,
    var quantite: Int,
    var prix: String,
    @ColumnInfo(name = "date_peremption")
    var datePeremption: Date,
    @ColumnInfo(name = "date_achat")
    var dateAchat: Date = Date(),
    @ColumnInfo(name = "est_en_stock")
    var estEnStock: Boolean = true,
    @ColumnInfo(name = "est_a_acheter")
    var estAAcheter: Boolean = false,
    @ColumnInfo(name = "note_nutrition")
    var noteNutrition: String = ""
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}