package androidkotlin.bryce.nooggy_v2.data

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

//Les DAO (Data Access Object) sont des interfaces qui vont nous permettent de communiquer et d’interagir
// avec notre base de données. C’est ici que nous allons implémenter nos méthodes dites CRUD (create, read, update et delete).
// il s'agit des actions de bases que l'on fait sur la BDD

@Dao
interface ProduitDao {

    //@Insert: Persister un objet en base de données.
    // on insère un produit dans la table
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduit(produit: Produit)

    // on met à jour un produit, on le met dans Achat
    @Query("UPDATE produits SET est_a_acheter = 1 WHERE id = :id ")
    fun addToAchat(id: Int)

    // on met à jour un produit, on le met dans Stock
    @Query("UPDATE produits SET est_en_stock = 1 WHERE id = :id ")
    fun addToStock(id: Int)

    // on modifie le produit qui est en stock
    @Query("UPDATE produits SET quantite = :quantite, prix = :prix, date_peremption = :date_Peremption WHERE id = :id")
    fun updateProduit(id: Int, quantite: Int, prix: String, date_Peremption: Date)


    // récupère tous les produits de la table
    // @Query(« requête »): Opération de lecture d’enregistrement via la définition de requête SQL
    @Query("SELECT * FROM produits ORDER BY date_achat DESC")
    fun getAllProduits(): LiveData<List<Produit>>

    // récupère tous les produits de Stock
    @Query("SELECT * FROM produits WHERE est_en_stock = 1 AND quantite > 0 ORDER BY CASE WHEN date_peremption IS 0 THEN 1 ELSE 0 END, date_peremption")
    fun getAllStock(): LiveData<List<Produit>>

    @Query("SELECT * FROM produits WHERE est_en_stock = 1 AND quantite > 0")
    suspend fun getAllStockList(): List<Produit>

    // récupère tous les produits de Achat
    @Query("SELECT * FROM produits WHERE est_a_acheter = 1 ORDER BY date_achat DESC")
    fun getAllAchats(): LiveData<List<Produit>>

    // récupère le prix du produit le plus récent au nom correspondant
    @Query("SELECT prix FROM produits WHERE id =(SELECT id FROM produits WHERE nom = :name ORDER BY id DESC LIMIT 1) ")
    fun getPrixByName(name: String): String

    // Permet de trier par ordre alpha avec le nom
    @Query("SELECT * FROM produits ORDER BY nom ASC")
    fun sortByName(): LiveData<List<Produit>>

    // permet de trier par marque
    @Query("SELECT * FROM produits ORDER BY CASE WHEN nom_marque = '' THEN 1 ELSE 0 END, nom_marque ASC")
    fun sortByBrand(): LiveData<List<Produit>>

    // Permet de trier par date de permeption du plus ancien
    @Query("SELECT * FROM produits ORDER BY CASE WHEN date_peremption IS 0 THEN 1 ELSE 0 END, date_peremption")
    fun sortByExpirationDate(): LiveData<List<Produit>>


    // @Delete: Suppression d’un objet persisté
    @Delete
    fun deleteProduit(produit: Produit)

    // supprime du stock
    @Query("UPDATE produits SET est_en_stock = 0 WHERE id = :id")
    fun deleteFromStock(id: Int)

    // supprime de Achat
    @Query("UPDATE produits SET est_a_acheter = 0 WHERE id = :id")
    fun deleteToAchat(id: Int)

    // Supprime de Achat par nom (quand scan du produit à racheter pour stock)
    @Query("UPDATE produits SET est_a_acheter = 0 WHERE nom LIKE :name")
    fun deleteToAchatByName(name: String)


}