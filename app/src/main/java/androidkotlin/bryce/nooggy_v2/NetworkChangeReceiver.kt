package androidkotlin.bryce.nooggy_v2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.widget.Toast

class NetworkChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        if (!isNetworkAvailable(context)) {
            Toast.makeText(context, "Aucune connexion internet", Toast.LENGTH_LONG).show()
        } else if (!App.firstLaunch) {
            Toast.makeText(context, "Connexion internet retrouvée", Toast.LENGTH_LONG).show()
        } else {
            App.firstLaunch = false
        }

    }

    fun isNetworkAvailable(context: Context):Boolean{
        val connectivityManager= context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo

        return networkInfo!=null && networkInfo.isConnected
    }

}