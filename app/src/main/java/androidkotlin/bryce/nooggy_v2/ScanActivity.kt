package androidkotlin.bryce.nooggy_v2

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidkotlin.bryce.nooggy_v2.util.ConverterUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_scan.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InterruptedIOException
import java.net.URL
import java.util.*


class ScanActivity : AppCompatActivity() {


    lateinit var mViewModel: MyViewModel
    private var isItemScanned = false

    private var nomProduit = ""
    private var marqueProduit = ""
    private var urlImageProduit = ""
    private var datePeremptionProduit = App.NULL_DATE
    private var quantiteProduit = 1
    private var prixProduit = "Prix :  €"
    private var noteNutritionProduit = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startScannerActivity()

        setContentView(R.layout.activity_scan)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProvider(this).get(MyViewModel::class.java)

        scan_activity_scan_button.setOnClickListener {
            startScannerActivity()
        }

        add_button_scan.setOnClickListener {
            if (isItemScanned) {
                val addDialog = Dialog(it.context, R.style.DialogStyle)
                addDialog.setContentView(R.layout.add_dialog)
                addDialog.setCancelable(true)
                addDialog.show()

                val prixBdd = mViewModel.getPrixByName(nomProduit)
                if (prixBdd != null) {
                    addDialog.findViewById<TextView>(R.id.prix_add_dialog).text =
                        prixBdd.split(":")[1].trimEnd('€').trim()
                }

                addDialog.findViewById<TextView>(R.id.date_scan_add).setOnClickListener {
                    val calendar = Calendar.getInstance()
                    val onDatePicked = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        calendar.set(Calendar.YEAR, year)
                        calendar.set(Calendar.MONTH, monthOfYear)
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        datePeremptionProduit = calendar.time
                        it.findViewById<TextView>(R.id.date_scan_add).text =
                            SimpleDateFormat("dd/MM/yyyy").format(datePeremptionProduit)
                    }
                    val datePickerDialog = DatePickerDialog(
                        this, onDatePicked,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
                    )
                    // afficher la dialog
                    datePickerDialog.show()
                }

                addDialog.findViewById<View>(R.id.bouton_add_final_dialog_scan).setOnClickListener {
                    try {
                        quantiteProduit =
                            addDialog.findViewById<EditText>(R.id.quantite_add_dialog).text.toString().toInt()
                    } catch (e: NumberFormatException) {
                        quantiteProduit = 1
                    }
                    prixProduit = "Prix : ${addDialog.findViewById<EditText>(R.id.prix_add_dialog).text} €"
                    val produit = Produit(
                        nomProduit,
                        marqueProduit,
                        urlImageProduit,
                        quantiteProduit,
                        prixProduit,
                        datePeremptionProduit,
                        noteNutrition = noteNutritionProduit
                    )

                    mViewModel.addFromScan(produit)
                    Toast.makeText(it.context, "Produit ajouté", Toast.LENGTH_SHORT).show()

                    //Vide les infos sur le produit
                    emptyInfoProduit()
                    isItemScanned = false
                    addDialog.dismiss()
                }
            }
        }
    }

    private fun startScannerActivity() {
        IntentIntegrator(this).apply {
            setPrompt("Scannez votre produit")
            setBeepEnabled(false)
            setOrientationLocked(true)
            initiateScan()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Aucun produit scanné", Toast.LENGTH_LONG).show()
                if (nom_produit.text == "") {
                    empty_state_scan.visibility = View.VISIBLE
                }
            } else {
                empty_state_scan.visibility = View.GONE
                val code = result.contents

                val resultat = runBlocking {
                    withContext(IO) {
                        try {
                            URL("https://fr.openfoodfacts.org/api/v0/produit/$code.json").readText()
                        } catch (e: IOException) {
                            // On retourne un résultat JSON avec un status égal à -1
                            "{\"status\":-1}"
                        } catch (e: InterruptedIOException) {
                            // On retourne un résultat JSON avec un status égal à -1
                            "{\"status\":-1}"
                        }
                    }
                }
                parseJson(resultat)
            }


        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun parseJson(resultatJson: String) {
        if (JSONObject(resultatJson).getString("status") == "1") {
            val productObjet = JSONObject(resultatJson).getJSONObject("product")

            val nom = try {
                productObjet.getString("product_name_fr")
            } catch (e: JSONException) {
                try {
                    productObjet.getString("product_name")
                } catch (e: JSONException) {
                    "Nom inconnu"
                }
            }
            val marque: String = try {
                productObjet.getString("brands")
            } catch (e: JSONException) {
                "Marque inconnue"
            }
            val urlImage = try {
                productObjet.getString("image_front_url")
            } catch (e: JSONException) {
                getString(R.string.image_not_found_url)
            }
            val noteNutrition = try {
                productObjet.getString("nutriscore_grade")
            } catch (e: JSONException) {
                try {
                    productObjet.getString("nutrition_grade_fr_producer")
                } catch (e: JSONException) {
                    ""
                }
            }

            displayInfoProduit(nom, marque, urlImage, noteNutrition)

        } else if (JSONObject(resultatJson).getString("status") == "0") {
            // Si le statut est à 0, on affiche ce Toast
            Toast.makeText(this, "Produit non trouvé", Toast.LENGTH_SHORT).show()
        } else {
            // Sinon (le statut n'est ni à 1 ni à 0)
            Toast.makeText(this, "Erreur de connexion avec le serveur", Toast.LENGTH_SHORT).show()
        }

    }

    private fun emptyInfoProduit() {
        image_produit.setImageResource(0)
        nom_produit.text = ""
        marque_produit.text = ""
        note_nutrition_bouton.background.setTint(Color.TRANSPARENT)
        empty_state_scan.visibility = View.VISIBLE
    }

    private fun displayInfoProduit(nom: String, marque: String, urlImage: String, noteNutrition: String) {
        isItemScanned = true

        nomProduit = nom
        marqueProduit = marque
        urlImageProduit = urlImage
        noteNutritionProduit = noteNutrition
        runOnUiThread {
            nom_produit.text = nom
            marque_produit.text = marque
            note_nutrition_bouton.background.setTint(ConverterUtils.fromGradeToColor(noteNutrition))

            val options = RequestOptions()
            options.placeholder(R.drawable.placeholder)
            Glide.with(this).setDefaultRequestOptions(options).load(urlImage).into(image_produit)
        }
    }
}