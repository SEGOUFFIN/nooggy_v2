package androidkotlin.bryce.nooggy_v2

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    val networkChangeReceiver = NetworkChangeReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)


    // Acces aux Layout de chaques Pages

        button_stock.setOnClickListener {
            val intent = Intent (this, StockActivity::class.java)
            startActivity(intent)
        }

        button_achat.setOnClickListener {
            val intent = Intent (this, AchatActivity::class.java)
            startActivity(intent)
        }


        button_scan.setOnClickListener {
            val intent = Intent (this, ScanActivity::class.java)
            // Librairie pour automatiser la permission CAMERA (QuickPermissions)
            runWithPermissions(Manifest.permission.CAMERA) {
                startActivity(intent)
            }

         }

        button_stats.setOnClickListener {
            val intent = Intent (this, StatsActivity::class.java)
            startActivity(intent)
        }
        registerReceiver(networkChangeReceiver, IntentFilter().apply {addAction("android.net.conn.CONNECTIVITY_CHANGE")})
    }



    override fun onPause() {
        super.onPause()
        App.firstLaunch = true
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(networkChangeReceiver)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.home_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home_item1 -> {
                val addDialog = Dialog(this, R.style.DialogStyle)
                addDialog.setContentView(R.layout.info_dialog)
                addDialog.setCancelable(true)
                addDialog.show()

                addDialog.findViewById<View>(R.id.btn_close_confidentialite).setOnClickListener {
                    addDialog.dismiss()
                }

                return true
            }

            R.id.home_item2 -> {
                val addDialog = Dialog(this, R.style.DialogStyle)
                addDialog.setContentView(R.layout.confidentialite_dialog)
                addDialog.setCancelable(true)
                addDialog.show()

                addDialog.findViewById<View>(R.id.btn_close_confidentialite).setOnClickListener {
                    addDialog.dismiss()
                }

                return true
            }

            R.id.home_item3 -> {
                val addDialog = Dialog(this, R.style.DialogStyle)
                addDialog.setContentView(R.layout.sauvegarde_dialog)
                addDialog.setCancelable(true)
                addDialog.show()

                addDialog.findViewById<View>(R.id.btn_close_confidentialite).setOnClickListener {
                    addDialog.dismiss()
                }

                return true}

            else -> return super.onOptionsItemSelected(item)
        }
    }



}
