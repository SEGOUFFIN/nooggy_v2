package androidkotlin.bryce.nooggy_v2

import androidx.lifecycle.ViewModel
import androidkotlin.bryce.nooggy_v2.data.Produit
import androidkotlin.bryce.nooggy_v2.data.ProduitRepository

// : il hérite de ViewModel
class MyViewModel : ViewModel() {
    // mRepository de type ProduitRepository
    private val mRepository: ProduitRepository = ProduitRepository()

    fun getAllStock() = mRepository.getAllStock()

    fun getAllAchats() = mRepository.getAllAchats()

    fun getAllProduits(method: ProduitRepository.SortingMethod) = mRepository.getAllProduits(method)

    fun getPrixByName(name: String): String = mRepository.getPrixByName(name)


    fun updateProduit(produit: Produit) {
        mRepository.updateProduit(produit)
    }

    fun addToAchatFromStock(id: Int) {
        mRepository.addToAchatFromStock(id)
    }

    fun addToStockFromStats(id: Int) {
        mRepository.addToStockFromStats(id)
    }

    fun addToAchat(id: Int) {
        mRepository.addToAchat(id)
    }

    fun addFromScan (produit: Produit) {
        mRepository.addFromScan(produit)
    }

    fun deleteToAchat(id: Int) {
        mRepository.deleteToAchat(id)
    }

    fun deleteFromStock(id: Int) {
        mRepository.deleteFromStock(id)
    }


    fun deleteProduit(produit: Produit) {
        mRepository.deleteProduit(produit)
    }

}