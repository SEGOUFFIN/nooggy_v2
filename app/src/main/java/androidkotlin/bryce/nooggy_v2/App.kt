package androidkotlin.bryce.nooggy_v2

import android.app.Application
import androidkotlin.bryce.nooggy_v2.data.NooggyDatabase
import androidkotlin.bryce.nooggy_v2.notification.NooggyNotificationManager
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import net.danlew.android.joda.JodaTimeAndroid
import java.util.*

class App : Application() {


    companion object {
        // pour être s^r qu'on ait qu'une seule instance
        lateinit var database: NooggyDatabase

        // constante pour être utilisée par tout
        val NULL_DATE = Date(0)

        // pour s'assurer que le message de surveillance de réseau ne s'initialise pas au démarrage
        var firstLaunch = true

    }

    override fun onCreate() {
        super.onCreate()

        val MIGRATION_4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE produits ADD COLUMN note_nutrition TEXT NOT NULL DEFAULT ''")
            }
        }

        // Déclaration de la bdd
        database = Room.databaseBuilder(this, NooggyDatabase::class.java, "nooggy_database")
            .addMigrations(MIGRATION_4_5)
            .build()

        //Initialisation de la librairie Joda
        //Attention: à initialiser avant de lancer le worker pour les notification d'expiration
        JodaTimeAndroid.init(this)

        // Ajoute la tâche récurrente d'envoi de notifications pour les produits périmés
        NooggyNotificationManager.schedulePeriodicNotifications(this)
    }

}